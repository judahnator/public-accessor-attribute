<?php

declare(strict_types=1);

namespace judahnator\PublicAccessorAttribute\Tests;

use judahnator\PublicAccessorAttribute\HasReadonlyProperties;
use judahnator\PublicAccessorAttribute\ReadOnly;
use PHPUnit\Framework\TestCase;

final class HasReadOnlyPropertiesTest extends TestCase
{
    /**
     * @covers \judahnator\PublicAccessorAttribute\HasReadonlyProperties::__call
     */
    public function testBadMethodName(): void
    {
        $testClass = new class {
            use HasReadonlyProperties;
        };

        $this->expectException(\ErrorException::class);
        $this->expectExceptionMessage(sprintf('Call to undefined method %s::%s', $testClass::class, 'foo'));

        $testClass->foo();
    }

    /**
     * @covers \judahnator\PublicAccessorAttribute\HasReadonlyProperties::__call
     * @covers \judahnator\PublicAccessorAttribute\HasReadonlyProperties::getAttribute
     */
    public function testNullWhenPropertyNotFound(): void
    {
        $testClass = new class {
            use HasReadonlyProperties;
            protected $foo = 'asdf';
        };

        $this->assertNull($testClass->getBarAttribute());
    }

    /**
     * @covers \judahnator\PublicAccessorAttribute\HasReadonlyProperties::__call
     * @covers \judahnator\PublicAccessorAttribute\HasReadonlyProperties::getAttribute
     */
    public function testNullWhenPropertyFoundButNoAttribute(): void
    {
        $testClass = new class {
            use HasReadonlyProperties;
            protected $foo = 'asdf';
        };

        $this->assertNull($testClass->getFooAttribute());
    }

    /**
     * @covers \judahnator\PublicAccessorAttribute\HasReadonlyProperties::__call
     * @covers \judahnator\PublicAccessorAttribute\HasReadonlyProperties::getAttribute
     */
    public function testBadPropertyVisibility(): void
    {
        $testClass = new class {
            use HasReadonlyProperties;
            private $foo = 'bar';
        };

        $this->expectException(\ErrorException::class);
        $this->expectExceptionMessage(sprintf('Cannot access private property %s::$%s.', $testClass::class, 'foo'));

        $testClass->getFooAttribute();
    }

    /**
     * @covers \judahnator\PublicAccessorAttribute\HasReadonlyProperties::getAttribute
     */
    public function testIfPropertyDoesNotHaveAttribute(): void
    {
        $testClass = new class {
            use HasReadonlyProperties;
            protected $foo = 'bar';
        };

        $this->assertNull($testClass->getAttribute('baz'));
    }

    /**
     * @covers \judahnator\PublicAccessorAttribute\HasReadonlyProperties::__call
     * @covers \judahnator\PublicAccessorAttribute\HasReadonlyProperties::getAttribute
     */
    public function testFetchingAttribute(): void
    {
        $testClass = new class {
            use HasReadonlyProperties;

            #[ReadOnly] protected $foo = 'bar';

            #[ReadOnly] public $barBaz = 'bong';
        };

        $this->assertEquals('bar', $testClass->getFooAttribute());
        $this->assertEquals('bong', $testClass->getBarBazAttribute());

        $this->assertEquals('bar', $testClass->getAttribute('foo'));
        $this->assertEquals('bong', $testClass->getAttribute('barBaz'));
    }
}
