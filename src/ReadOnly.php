<?php

declare(strict_types=1);

namespace judahnator\PublicAccessorAttribute;

use Attribute;

#[Attribute(Attribute::TARGET_PROPERTY)]
final class ReadOnly {}