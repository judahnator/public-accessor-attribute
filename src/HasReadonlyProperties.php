<?php

declare(strict_types=1);

namespace judahnator\PublicAccessorAttribute;

use ErrorException;
use ReflectionClass;
use ReflectionException;

trait HasReadonlyProperties
{
    /**
     * @param string $name
     * @param array $arguments
     * @return mixed|null
     * @throws ErrorException
     */
    public function __call(string $name, array $arguments = [])
    {
        // Finds the name of the attribute we want
        $property = preg_replace('/^get([A-Z]\w+)Attribute$/', '$1', $name);

        // If the above returns unchanged then the pattern did not match, give an error.
        if ($property === $name) {
            throw new ErrorException(sprintf('Call to undefined method %s::%s', static::class, $name));
        }

        // Properties are not typically capitalized, so drop the case of the first letter.
        $property[0] = strtolower($property[0]);

        return $this->getAttribute($property);
    }

    /**
     * @param string $property
     * @return mixed|null
     * @throws ErrorException
     */
    public function getAttribute(string $property)
    {
        // Get the reflected property of the class, if the property does not exist return null
        try {
            $reflected = (new ReflectionClass($this))->getProperty($property);
        } catch (ReflectionException) {
            return null;
        }

        // We do not want to allow access to private properties.
        if ($reflected->isPrivate()) {
            throw new ErrorException(sprintf('Cannot access private property %s::$%s.', static::class, $property));
        }

        // Only return the value if the property has the ReadOnly attribute.
        if ($reflected->getAttributes(ReadOnly::class)) {
            return $this->{$property};
        }

        return null;
    }
}
